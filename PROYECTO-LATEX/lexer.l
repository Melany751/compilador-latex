/* Programa que reconoce palabras utilizando una 
tabla de simbolos */

/* Sección DEFINICIONES */
%{
#include <stdio.h>
#include <string.h>
#include "parser.tab.h" //para que lexer.l y parser.y usen las mismas constantes
#define BUSCAR 0

int estado;

int agregar_palabra(int tipo, char *palabra);
int buscar_palabra(char *palabra);
int agregar_lexemas();
%}

/* Sección REGLAS */
%%

    /*      \n    		{estado = BUSCAR;} retornar a estado default*/
\\          {agregar_palabra(SIMBOLO_APERTURA,yytext); return SIMBOLO_APERTURA;}
("%".*)    	{agregar_palabra(COMENTARIO, yytext);return COMENTARIO;}

"(" 	    {agregar_palabra(PARENTESIS_AP, yytext); return PARENTESIS_AP;}
"["  	{agregar_palabra(CORCHETE_AP, yytext); return CORCHETE_AP;}
"{"		{agregar_palabra(LLAVE_AP, yytext); return LLAVE_AP;}
"/"			{agregar_palabra(SLASH, yytext); return SLASH;}
")"  	{agregar_palabra(PARENTESIS_CE, yytext); return PARENTESIS_CE;}
"]"		{agregar_palabra(CORCHETE_CE, yytext); return CORCHETE_CE;}
"}"     		{agregar_palabra(LLAVE_CE, yytext); return LLAVE_CE;}
"="			{agregar_palabra(IGUAL, yytext); return IGUAL;} 
","  		{agregar_palabra(COMA, yytext); return COMA;} /* VERIFICAR!!!!*/

\\begin  		{agregar_palabra(BEGINING, yytext); return BEGINING;}
\\end   		{agregar_palabra(END, yytext); return END;}
\\markright 		{agregar_palabra(MARKRIGHT, yytext); return MARKRIGHT;}
\\newpage 		{agregar_palabra(NEWPAGE, yytext); return NEWPAGE;}
\\usepackage  		{agregar_palabra(USEPACKAGE, yytext); return USEPACKAGE;}
\\atbegindocument	{agregar_palabra(ATBEGINDOCUMENT, yytext); return ATBEGINDOCUMENT;}
\\documentclass	      	{agregar_palabra(DOCUMENTCLASS, yytext); return DOCUMENTCLASS;printf("documentclass lexer...\n");}
\\renewcommand   	{agregar_palabra(RENEWCOMMAND, yytext); return RENEWCOMMAND;}
\\title   		{agregar_palabra(TITLE, yytext); return TITLE;}
\\author 		{agregar_palabra(AUTHOR, yytext); return AUTHOR;}
\\date 			{agregar_palabra(DATE, yytext); return DATE;}
article|book|report|theorem|verse|verbatim {agregar_palabra(TIPO_DOCUMENTO, yytext); return TIPO_DOCUMENTO;}
document 			{agregar_palabra(DOCUMENTO, yytext); return DOCUMENTO;}

a4paper|12pt|11pt|a2paper {agregar_palabra(FORMATO_DOCUMENTO, yytext); return FORMATO_DOCUMENTO;}
abstract|amsmath|amssymb|amsfonts|array|fancyhdr|enumerate|graphics|graphicx|hyperref|latexsym|listings|lmodern|pdflscape|textcomp|xcolor|xypic {agregar_palabra(PAQUETES, yytext); return PAQUETES;}
equation  {agregar_palabra(ECUACION, yytext); return ECUACION;}
babel      		{agregar_palabra(P_BABEL, yytext); return P_BABEL;}
fontenc   		{agregar_palabra(P_FONTENC, yytext); return P_FONTENC;}
inputenc   		{agregar_palabra(P_INPUTENC, yytext); return P_INPUTENC;}
T1		{agregar_palabra(VALOR_FUENTE, yytext); return VALOR_FUENTE;}
utf8 		{agregar_palabra(UNICODE, yytext); return UNICODE;}
spanish {agregar_palabra(IDIOMA_ESPANOL, yytext); return IDIOMA_ESPANOL;}
itemize {agregar_palabra(ITEMIZE, yytext); return ITEMIZE;}
\\item {agregar_palabra(ITEM, yytext); return ITEM;}
es-tabla|es-nondecimaldot|es-lcroman|es-noidentfirst {agregar_palabra(CONSIDERACIONES_IDIOMA_ESPANOL, yytext); return CONSIDERACIONES_IDIOMA_ESPANOL;}
pagestyle      	{agregar_palabra(ESTILO_PAG, yytext); return ESTILO_PAG;}

chapter 		{agregar_palabra(CHAPTER, yytext); return CHAPTER;}
section 		{agregar_palabra(SECTION, yytext); return SECTION;}
subsection  		{agregar_palabra(SUBSECTION, yytext); return SUBSECTION;}
subsubsection		{agregar_palabra(SUBSUBSECTION, yytext); return SUBSUBSECTION;}
paragraph	      	{agregar_palabra(PARAGRAPH, yytext); return PARAGRAPH;}
subparagraph   	{agregar_palabra(SUBPARAGRAPH, yytext); return SUBPARAGRAPH;}
part   		{agregar_palabra(PART, yytext); return PART;}

includegraphics   	{agregar_palabra(INCLUDEGRAPHICS, yytext); return INCLUDEGRAPHICS;}
figure   		{agregar_palabra(FIGURE, yytext); return FIGURE;}
subfigure 		{agregar_palabra(SUBFIGURE, yytext); return SUBFIGURE;}
width|height|scale|angle {agregar_palabra(OPCIONES, yytext); return OPCIONES;}
h|b|t|p 		{agregar_palabra(LUGAR, yytext); return LUGAR;}
caption		{agregar_palabra(CAPTION, yytext); return CAPTION;}
centering   		{agregar_palabra(CENTERING, yytext); return CENTERING;}
label   		{agregar_palabra(LABEL, yytext); return LABEL;}

table 		{agregar_palabra(TABLAS, yytext); return TABLAS;}
tabular         {agregar_palabra(PAQ_TABLAS, yytext); return PAQ_TABLAS;}
\\hline           {agregar_palabra(HLINE, yytext); return HLINE;}
endfirsthead    {agregar_palabra(ENDFIRSTHEAD, yytext); return ENDFIRSTHEAD;}
endhead         {agregar_palabra(ENDHEAD, yytext); return ENDHEAD;}
longtable       {agregar_palabra(LONGTABLE, yytext); return LONGTABLE;}
multicolumn     {agregar_palabra(MULTICOLUMN, yytext); return MULTICOLUMN;}
multirow        {agregar_palabra(MULTIROW, yytext); return MULTIROW;}
&               {agregar_palabra(SEPARADOR, yytext); return SEPARADOR;}
"|"              {agregar_palabra(LINEA_TABLA, yytext); return LINEA_TABLA;}
r|l|c               {{agregar_palabra(CABECERA_TABLA, yytext);} return CABECERA_TABLA;} 
  
alpha|beta|gamma|delta|epsilon|zeta|theta|lambda|pi|sigma|omega|chi {agregar_palabra(ALFABETO_GRIEGO, yytext); return ALFABETO_GRIEGO;}
pm|times|div|"+"|-|cup|cap|ast|cdot {agregar_palabra(OPERACIONES, yytext); return OPERACIONES;}
sqrt                {agregar_palabra(RAICES, yytext); return RAICES;}
frac|dfrac          {agregar_palabra(FRACCIONES, yytext); return FRACCIONES;}
leq|prec|preceq|subset|subseteq {agregar_palabra(OPER_RELACION, yytext); return OPER_RELACION;}
"<"|">"               {agregar_palabra(DESIGUALDADES, yytext); return DESIGUALDADES;}     
"^"   	{agregar_palabra(SUPERINDICE, yytext); return SUPERINDICE;}
"_"     {agregar_palabra(SUBINDICE, yytext);return SUBINDICE;}
raz_trig 		{agregar_palabra(RAZ_TRIG, yytext); return RAZ_TRIG;}
angulo 		{agregar_palabra(ANGULO, yytext); return ANGULO;}
"$"               {agregar_palabra(ECUACIONENLINEA, yytext); return ECUACIONENLINEA;}

rm|em|bf|it|sf|sc|tt|\\underline|\\textbf|\\textit|\\textdagger {agregar_palabra(FORMATEXTO, yytext); return FORMATEXTO;}
textheight|textwidth|topmargin|oddsidemargin|evensidemargin|headheight|headsep|columnsep|marginparwidth {agregar_palabra(MARGENES, yytext); return MARGENES;}
raggedleft|raggedright  {agregar_palabra(ALINEADO, yytext); return ALINEADO;}
\\[parskip|parindent] {agregar_palabra(SANGRIA, yytext); return SANGRIA;}
mm|cm|in|pt|pc  {agregar_palabra(UNIDADES, yytext); return UNIDADES;}
([0-9]|[a-zA-Z]|á|é|í|ó|ú|([ ])|"."|",")+ {agregar_palabra(SECUENCIA, yytext); return SECUENCIA;}
\\[a-z]+    {printf("Secuencia incorrecta...\n");}
.	

%%

/* Sección CODIGO USUARIO */

/* definir una lista enlazada de palabras y tipos*/
struct palabra {
    char *nombre_palabra;
    int tipo_palabra;
    struct palabra *sgte;
};

struct palabra *lista = NULL; /*primer elemento de la lista*/

int agregar_lexemas() {
    agregar_palabra(SIMBOLO_APERTURA, "\\");
	agregar_palabra(COMENTARIO, "%");
		
    return 0;
}

int agregar_palabra(int tipo, char *palabra) {
    struct palabra *p; /*entrada de la lista*/
    
    if ( buscar_palabra(palabra) != BUSCAR ) {
        //printf("La palabra %s ya fue definida!!!\n", palabra);
        return 0;
    }
    /* asignar espacio de memoria */
    p = malloc( sizeof( struct palabra ) );
    p->nombre_palabra = malloc( strlen(palabra) + 1 ); 
    /* copiar datos de palabra */
    strcpy( p->nombre_palabra, palabra );
    p->tipo_palabra = tipo;    
    /* enlazar nueva entrada a lista */
    p->sgte = lista;
    lista = p;
    
    FILE *fichero;
    fichero = fopen("arreglo.txt","w+");
    fwrite(lista, sizeof(char), sizeof(lista), fichero );
    fclose(fichero);    

    return 1;
}

int buscar_palabra(char *palabra){
    struct palabra *p = lista;
    
    /* buscar palabra en la lista */
    while (p != NULL) {
        if ( strcmp(p->nombre_palabra, palabra) == 0 )
            return p->tipo_palabra;
        p = p->sgte;
    }
    
    return BUSCAR; /*no encontrado*/
}

/*char retornar_palabra(int tipo){
    struct palabra *p = lista;
    
    /* buscar palabra en la lista 
    while (p != NULL) {
        if ( strcmp(p->tipo_palabra, tipo) == 0 )
            return p->nombre_palabra;
        p = p->sgte;
    }
    
    return BUSCAR; /*no encontrado

}*/

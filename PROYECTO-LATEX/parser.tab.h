/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_PARSER_TAB_H_INCLUDED
# define YY_YY_PARSER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    SIMBOLO_APERTURA = 258,
    COMENTARIO = 259,
    PARENTESIS_AP = 260,
    CORCHETE_AP = 261,
    LLAVE_AP = 262,
    SLASH = 263,
    PARENTESIS_CE = 264,
    CORCHETE_CE = 265,
    LLAVE_CE = 266,
    IGUAL = 267,
    COMA = 268,
    SECUENCIA = 269,
    NUMEROS = 270,
    SIGNOS = 271,
    BEGINING = 272,
    END = 273,
    MARKRIGHT = 274,
    NEWPAGE = 275,
    USEPACKAGE = 276,
    ATBEGINDOCUMENT = 277,
    DOCUMENTCLASS = 278,
    RENEWCOMMAND = 279,
    TITLE = 280,
    AUTHOR = 281,
    DATE = 282,
    TIPO_DOCUMENTO = 283,
    FORMATO_DOCUMENTO = 284,
    PAQUETES = 285,
    P_BABEL = 286,
    P_FONTENC = 287,
    P_INPUTENC = 288,
    VALOR_FUENTE = 289,
    UNICODE = 290,
    IDIOMA_ESPANOL = 291,
    CONSIDERACIONES_IDIOMA_ESPANOL = 292,
    ESTILO_PAG = 293,
    CHAPTER = 294,
    SECTION = 295,
    SUBSECTION = 296,
    SUBSUBSECTION = 297,
    PARAGRAPH = 298,
    SUBPARAGRAPH = 299,
    PART = 300,
    INCLUDEGRAPHICS = 301,
    FIGURE = 302,
    SUBFIGURE = 303,
    OPCIONES = 304,
    LUGAR = 305,
    CAPTION = 306,
    CENTERING = 307,
    LABEL = 308,
    TABLAS = 309,
    PAQ_TABLAS = 310,
    HLINE = 311,
    ENDFIRSTHEAD = 312,
    ENDHEAD = 313,
    LONGTABLE = 314,
    MULTICOLUMN = 315,
    MULTIROW = 316,
    SEPARADOR = 317,
    LINEA_TABLA = 318,
    CABECERA_TABLA = 319,
    ALFABETO_GRIEGO = 320,
    OPERACIONES = 321,
    RAICES = 322,
    FRACCIONES = 323,
    OPER_RELACION = 324,
    DESIGUALDADES = 325,
    SUPERINDICE = 326,
    SUBINDICE = 327,
    RAZ_TRIG = 328,
    ANGULO = 329,
    ECUACIONENLINEA = 330,
    FORMATEXTO = 331,
    MARGENES = 332,
    ALINEADO = 333,
    SANGRIA = 334,
    UNIDADES = 335,
    ECUACION = 336,
    DOCUMENTO = 337,
    ITEMIZE = 338,
    ITEM = 339
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_TAB_H_INCLUDED  */
